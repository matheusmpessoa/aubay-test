# aubay-test

## Decision
The decision I made was to make the main functionality of the test, which was to make an application that returns character data when searched by name, with a simple visual presentation.

## Possible improvements
- Use icons on characteristics of character
- Improve layout
- Improve constructors and functions

## Information of the project
Project built with React

- __[Create React App](https://github.com/facebook/create-react-app)__ to build a project immediately.
- __[Styled components](https://github.com/styled-components/styled-components)__ to style project.
- __[Swapi](https://swapi.co/)__ to get characters info.

## Execute
To run the project go to *aubay-test/my-app* and run the commands:

```js
npm install
```

and

```js
npm start
```
