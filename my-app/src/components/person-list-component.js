import React, { Component } from 'react'
import PersonItemComponent from './person-item-component';

export default class PersonListComponent extends Component {

   constructor(props) {
       super(props);
   }

   mountTrData(teste) {
        let td = teste.map((val, idx) => {
           let data = {
               name: val.name,
               height: val.height,
               mass: val.mass,
               hair_color: val.hair_color,
               skin_color: val.skin_color,
               eye_color: val.eye_color,
               birth_year: val.birth_year,
               gender: val.gender
           };
           return <PersonItemComponent key={idx} data={data} />;
       });
       return td;
   }

   render() {
       const tdData = this.mountTrData(this.props.dataPerson);
        return (
            <div>
                <table>
                   <thead>
                        <tr>
                            <th>Name</th>
                            <th>Height</th>
                            <th>Mass</th>
                            <th>Hair color</th>
                            <th>Skin color</th>
                            <th>Eye color</th>
                            <th>Birthday year</th>
                            <th>Gender</th>
                        </tr>
                    </thead>
                    <tbody>
                        { tdData }
                    </tbody>
                </table>
            </div>
        )
   }
}
