import React, { Component } from 'react'

export default class PersonItemComponent extends Component {
    constructor(props) {
      super(props);
    }

  render() {
    const {
      name,
      height,
      mass,
      hair_color,
      skin_color,
      eye_color,
      birth_year,
      gender
    } = this.props.data;
    return (
      <tr>
        <td>{ name }</td>
        <td>{ height }</td>
        <td>{ mass }</td>
        <td>{ hair_color }</td>
        <td>{ skin_color }</td>
        <td>{ eye_color }</td>
        <td>{ birth_year }</td>
        <td>{ gender }</td>
      </tr>
    )
  }
}
