import React, { Component } from 'react'
import styled from 'styled-components';

const ButtonSearch = styled.button`
 color: white;
 font-size: 14px;
 width: 90px;
 height: 36px;
 background-color: #009688;
`;

export default class SearchComponent extends Component {

  constructor(props) {
      super(props);
  }

  render() {
      const {query, handleSearch, handleChangeQuery} = this.props;
      return (
          <div>
              <input type="text" onChange={handleChangeQuery} placeholder="Type..."/>
              <ButtonSearch
               onClick={handleSearch}
               >Search</ButtonSearch>
          </div>
      )
  }
}
