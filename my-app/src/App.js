import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TopBarComponent from './components/top-bar-component';
import SearchComponent from './components/search-component';
import PersonListComponent from './components/person-list-component';
import BottomBarComponent from './components/bottom-bar-component';
import * as api from './endpoints';
import Axios from 'axios';

class App extends Component {
 constructor(props) {
   super(props);
   this.state = {
     teste: [
       {
         "name": "Luke Skywalker",
         "height": "172",
         "eye_color": "blue"
       }
     ],
     query: ''
   }
   this.handleSearch = this.handleSearch.bind(this);
   this.handleChangeQuery = this.handleChangeQuery.bind(this);
   this.getAllPersons = this.getAllPersons.bind(this);
   this.mountTable = this.mountTable.bind(this);
 }

 componentDidMount() {
   this.getAllPersons();
 }

 getAllPersons() {
   const self = this;
   Axios.get(api.urlGetAll)
     .then(function (response) {
       if (response.data) {
         let results = response.data.results;
         self.mountTable(results);
       }
     })
     .catch(function (error) {
       console.log(error);
     });
 }

 mountTable(results) {
   this.setState({teste: results})
 }

 handleSearch() {
   const self = this;
   const queryApi = `${api.urlGetBySearch}${this.state.query}`;
   Axios.get(queryApi)
     .then(function (response) {
       if (response.data) {
         let results = response.data.results;
         self.mountTable(results);
       }
     })
     .catch(function (error) {
       console.log(error);
     });
 }

 handleChangeQuery(e) {
   this.state.query = e.target.value;
 }

 render() {
   const {teste, query} = this.state;
  
   let container = <div><p>"Carregando...</p></div>;
  
   if (teste.length > 0) {
     container = <div className="App">
      <TopBarComponent></TopBarComponent>
       <SearchComponent
         handleSearch={this.handleSearch}
         query={query}
         handleChangeQuery={this.handleChangeQuery}
       ></SearchComponent>
       <PersonListComponent dataPerson={teste}></PersonListComponent>
       <BottomBarComponent></BottomBarComponent>
     </div>
   }
   return container;
 }
}

export default App;